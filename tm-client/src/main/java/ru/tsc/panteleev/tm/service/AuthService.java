package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.service.IAuthService;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.dto.model.UserDTO;

public class AuthService implements IAuthService {
    @Override
    public void login(@Nullable String login, @Nullable String password) {

    }

    @Override
    public void logout() {

    }

    @Override
    public void registry(@Nullable String login, @Nullable String password, @Nullable String email) {

    }

    @Override
    public @Nullable UserDTO getUser() {
        return null;
    }

    @Override
    public @NotNull String getUserId() {
        return null;
    }

    @Override
    public boolean isAuth() {
        return false;
    }

    @Override
    public void checkRoles(@Nullable Role[] roles) {

    }

    @Override
    public @NotNull UserDTO check(String login, String password) {
        return null;
    }

    /*
    @Nullable
    private final IPropertyService propertyService;

    @Nullable
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(
            @Nullable final IPropertyService propertyService,
            @Nullable final IUserService userService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        @NotNull final User user = check(login,password);
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        userService.create(login, password, email);
    }

    @Nullable
    @Override
    public User getUser() {
        return userService.findById(getUserId());
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @Nullable final User user = getUser();
        @NotNull final Role userRole = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(userRole);
        if (!hasRole) throw new PermissionException();
    }

    @NotNull
    @Override
    public User check(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new LoginOrPasswordIncorrectException();
        if (user.getLocked()) throw new LoginOrPasswordIncorrectException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new AccessDeniedException();
        if (!passwordHash.equals(user.getPasswordHash())) throw new LoginOrPasswordIncorrectException();
        return user;
    }*/
}
