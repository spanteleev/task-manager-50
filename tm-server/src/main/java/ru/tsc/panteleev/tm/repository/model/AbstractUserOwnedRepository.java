package ru.tsc.panteleev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.panteleev.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;

public class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

}
