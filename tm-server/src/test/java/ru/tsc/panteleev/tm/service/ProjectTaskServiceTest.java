package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.panteleev.tm.api.service.IConnectionService;
import ru.tsc.panteleev.tm.api.service.IPropertyService;
import ru.tsc.panteleev.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.panteleev.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.panteleev.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.panteleev.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.panteleev.tm.dto.model.ProjectDTO;
import ru.tsc.panteleev.tm.dto.model.TaskDTO;
import ru.tsc.panteleev.tm.dto.model.UserDTO;
import ru.tsc.panteleev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.panteleev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.panteleev.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.panteleev.tm.exception.field.TaskIdEmptyException;
import ru.tsc.panteleev.tm.service.dto.ProjectServiceDTO;
import ru.tsc.panteleev.tm.service.dto.ProjectTaskServiceDTO;
import ru.tsc.panteleev.tm.service.dto.TaskServiceDTO;
import ru.tsc.panteleev.tm.service.dto.UserServiceDTO;

import java.util.UUID;

public class ProjectTaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);

    @NotNull
    private final ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);

    @NotNull
    private final IProjectTaskServiceDTO projectTaskService = new ProjectTaskServiceDTO(connectionService);

    @NotNull
    private final IUserServiceDTO userService = new UserServiceDTO(connectionService, propertyService);

    @NotNull
    private final UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString());

    @NotNull
    private final static String STRING_RANDOM = UUID.randomUUID().toString();

    @NotNull
    private final static String STRING_EMPTY = "";

    @After
    public void end() {
        taskService.clear(user.getId());
        projectService.clear(user.getId());
    }

    @Test
    public void bindTaskToProject() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final ProjectDTO project = projectService.create(user.getId(), name, description, null, null);
        @NotNull final String taskName = UUID.randomUUID().toString();
        @NotNull final String taskDescription = UUID.randomUUID().toString();
        @NotNull final TaskDTO task = taskService.create(user.getId(), taskName, taskDescription, null, null);
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), STRING_EMPTY, STRING_RANDOM));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), STRING_RANDOM, STRING_EMPTY));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(user.getId(), STRING_RANDOM, task.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.bindTaskToProject(user.getId(), project.getId(), STRING_RANDOM));
        projectTaskService.bindTaskToProject(user.getId(), project.getId(), task.getId());
        @NotNull final TaskDTO taskAfterUpdate = taskService.findById(user.getId(), task.getId());
        Assert.assertSame(project.getId(), taskAfterUpdate.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final ProjectDTO project = projectService.create(user.getId(), name, description, null, null);
        @NotNull final String taskName = UUID.randomUUID().toString();
        @NotNull final String taskDescription = UUID.randomUUID().toString();
        @NotNull final TaskDTO task = taskService.create(user.getId(), taskName, taskDescription, null, null);
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), STRING_EMPTY, STRING_RANDOM));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), STRING_RANDOM, STRING_EMPTY));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), STRING_RANDOM, task.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), project.getId(), STRING_RANDOM));
        projectTaskService.bindTaskToProject(user.getId(), project.getId(), task.getId());
        @NotNull TaskDTO taskAfterUpdate = taskService.findById(user.getId(), task.getId());
        Assert.assertSame(project.getId(), taskAfterUpdate.getProjectId());
        projectTaskService.unbindTaskFromProject(user.getId(), project.getId(), task.getId());
        taskAfterUpdate = taskService.findById(user.getId(), task.getId());
        Assert.assertNull(taskAfterUpdate.getProjectId());
    }

    @Test
    public void removeProjectById() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final ProjectDTO project1 = projectService.create(user.getId(), name, description, null, null);
        @NotNull final ProjectDTO project2 = projectService.create(user.getId(), name, description, null, null);
        @NotNull final String taskName = UUID.randomUUID().toString();
        @NotNull final String taskDescription = UUID.randomUUID().toString();
        @NotNull final TaskDTO task1 = taskService.create(user.getId(), taskName, taskDescription, null, null);
        @NotNull final TaskDTO task2 = taskService.create(user.getId(), taskName, taskDescription, null, null);
        @NotNull final TaskDTO task3 = taskService.create(user.getId(), taskName, taskDescription, null, null);
        projectTaskService.bindTaskToProject(user.getId(), project1.getId(), task1.getId());
        projectTaskService.bindTaskToProject(user.getId(), project2.getId(), task2.getId());
        projectTaskService.bindTaskToProject(user.getId(), project2.getId(), task3.getId());
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(user.getId(), STRING_EMPTY));
        projectTaskService.removeProjectById(user.getId(), project2.getId());
        Assert.assertNotNull(taskService.findById(user.getId(), task1.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.findById(user.getId(), project2.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.findById(user.getId(), task2.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.findById(user.getId(), task3.getId()));
    }

}
